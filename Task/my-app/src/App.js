import React, { useState } from "react";

function App() {
  const [tasks, setTasks] = useState([]);
  const [formData, setFormData] = useState("");
  const [editTaskIndex, setEditTaskIndex] = useState(null);
  const [error, setError] = useState({
    tasks: "",
  });

  const handleTaskChange = (event) => {
    setFormData(event.target.value);
  };

  const handleSaveChanges = () => {
    if (!formData) {
      setError({ tasks: "Please enter task" });
    } else {
      setError({ tasks: "" });
    }
    if (formData.trim() !== "") {
      if (editTaskIndex === null) {
        setTasks([...tasks, formData]);
      } else {
        const updatedTasks = [...tasks];
        updatedTasks[editTaskIndex] = formData;
        setTasks(updatedTasks);
      }
      setEditTaskIndex(null);
      setFormData("");
    }
  };

  const handleEditTask = (index) => {
    setEditTaskIndex(index);
    setFormData(tasks[index]);
  };

  const handleDeleteTask = (index) => {
    const updatedTasks = tasks.filter((_, i) => i !== index);
    setTasks(updatedTasks);
  };

  return (
    <div className='container'>
      <h1>Todo List</h1>
      <button
        className='btn btn-primary mb-3'
        onClick={() => {
          setFormData("");
          setEditTaskIndex(null);
        }}
        data-bs-toggle='modal'
        data-bs-target='#editTaskModal'
      >
        Add
      </button>

      <ul className='list-group'>
        {tasks.map((task, index) => (
          <li
            key={index}
            className='list-group-item d-flex justify-content-between align-items-center'
          >
            {task}
            <div>
              <button
                className='btn btn-warning me-2'
                onClick={() => handleEditTask(index)}
                data-bs-toggle='modal'
                data-bs-target='#editTaskModal'
              >
                Edit
              </button>
              <button
                className='btn btn-danger'
                onClick={() => handleDeleteTask(index)}
              >
                Delete
              </button>
            </div>
          </li>
        ))}
      </ul>

      {/* Edit Task Modal */}
      <div
        className='modal fade'
        id='editTaskModal'
        tabIndex='-1'
        aria-labelledby='editTaskModalLabel'
        aria-hidden='true'
      >
        <div className='modal-dialog'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h5 className='modal-title' id='editTaskModalLabel'>
                Edit Task
              </h5>
              <button
                type='button'
                className='btn-close'
                data-bs-dismiss='modal'
                aria-label='Close'
              ></button>
            </div>
            <div className='modal-body'>
              <div className='form-floating'>
                <input
                  type='text'
                  className='form-control'
                  id='floatingInput'
                  placeholder='task...'
                  value={formData}
                  onChange={handleTaskChange}
                />
                <label htmlFor='floatingInput'>Task</label>
              </div>
              {formData ? (
                ""
              ) : (
                <span style={{ color: "red" }}>{error.tasks}</span>
              )}
            </div>
            <div className='modal-footer'>
              <button
                type='button'
                className='btn btn-secondary'
                data-bs-dismiss='modal'
              >
                Close
              </button>
              <button
                type='button'
                className='btn btn-primary'
                onClick={handleSaveChanges}
              >
                Save changes
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
