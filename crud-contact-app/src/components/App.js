import React, { useState, useEffect } from "react";
import { v4 as uuid } from "uuid";
import "./App.css";
import Header from "./Header";
import AddContact from "./AddContact";
import ContactList from "./ContactList";
import api from "../api/contacts"
// import ContactDetail from "./ContactDetail";
import { BrowserRouter, Switch, Route } from "react-router-dom";

function App() {
  // const LOCAL_STORAGE_KEY = "contacts";
  const [contacts, setContacts] = useState([]
    // JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY)) ?? []
    
  );
    // console.log(contacts);
  const retriveContacts = async () => {
    const responce = await api.get("/contacts");
    return responce.data;
  }


  const AddContactHandler = async (contact) => {
    const request = {
      id:uuid(),
      ...contact
    }
    console.log(request)
    const responce = await api.post("/contacts", request)
    // console.log(responce);
    setContacts([...contacts, responce.data]);
  };

  const removeContactHandler = async (id) => {
    await api.delete(`/contacts/${id}`);
    const newContactList = contacts.filter((contact) => {
      return contact.id !== id;
    });
    setContacts(newContactList);
  };

  // useEffect(() => {
  //   const retriveContacts = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
  //   if (retriveContacts) setContacts(retriveContacts);
  //   console.log("<<<<<<<<<<<<<<",retriveContacts)
  // }, []);

  useEffect(() => { 
    const getAllContacts = async () => {
      const allContacts = await retriveContacts();
      if(allContacts) setContacts(allContacts);
    }
    getAllContacts()
  }, []);

  // useEffect(() => {
  //   // localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(contacts));
  //   // console.log(contacts)
  // }, [contacts]);

  return (
    <div className="ui container">
      <BrowserRouter>
        <Header />
        <Switch>
          <Route exact path="/" render={(props) => (<ContactList {...props} contacts={contacts} getContactId={removeContactHandler} />)}
          />
          <Route
            exact
            path="/add"
            render={(props) => (<AddContact {...props} AddContactHandler={AddContactHandler} />)}
          />
          {/* <Route exact path="/contact/:id" componant={<ContactDetail  />} /> */}
          {/* <AddContact AddContactHandler={AddContactHandler} /> */}
          {/* <ContactList contacts={contacts} getContactId={removeContactHandler} /> */}
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
